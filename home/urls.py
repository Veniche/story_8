from django.urls import path
from . import views
from .views import MatkulView, MatkulDetailView, MatkulDeleteView, MatkulCreateView, ListKegiatanView, AddKegiatanView, AddPesertaView

app_name = 'home'

urlpatterns = [
    path('', views.index, name='index'),
    path('about', views.about, name='about'),
    path('project', views.project, name='project'),
    path('accordion', views.accordion, name='accordion'),
    path('search', views.search, name='search'),
    path('data', views.data, name='data'),
    path('matkul_create', MatkulCreateView.as_view(), name='matkul_create'),
    path('list', MatkulView.as_view(), name='list'),
    path('detail/<int:pk>', MatkulDetailView.as_view(), name='detail'),
    path('detail/<int:pk>/delete', MatkulDeleteView.as_view(), name='delete'),
    path('kegiatan', ListKegiatanView.as_view(), name='kegiatan'),
    path('kegiatan/add_kegiatan', AddKegiatanView.as_view(), name='AddKeg'),
    path('kegiatan/add_peserta', AddPesertaView.as_view(), name='AddOrg'),
]
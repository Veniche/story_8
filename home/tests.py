from django.test import TestCase, Client
from .models import Matkul, Kegiatan, Peserta

# Create your tests here.
class Test(TestCase):
    def test_model_Matkul(self):
        m = Matkul.objects.create(
            NamaMatkul="MatkulTest",
            Dosen="Tester",
            sks="3",
            deskripsi="TestTestTest",
            smst="Gasal 2020/2021",
            kelas="A.107")
        self.assertEquals('/list', m.get_absolute_url())

    def test_model_Kegiatan(self):
        k = Kegiatan.objects.create(NamaKegiatan="TestKeg")
        self.assertEquals(k.__str__(), k.NamaKegiatan)
        self.assertEquals('/kegiatan', k.get_absolute_url())

    def test_model_Peserta(self):
        k = Kegiatan.objects.create(NamaKegiatan="TestKeg")
        p = Peserta.objects.create(Nama="TestA", NamaKegiatan=k)
        self.assertEquals(p.__str__(), p.Nama)
        self.assertEquals('/kegiatan', p.get_absolute_url())

    def test_index_view(self):
        response = Client().get('')
        self.assertEquals(response.status_code, 200)

    def test_about_view(self):
        response = Client().get('/about')
        self.assertEquals(response.status_code, 200)

    def test_project_view(self):
        response = Client().get('/project')
        self.assertEquals(response.status_code, 200)

    def test_view_kegiatan(self):
        response = Client().get('/kegiatan')
        isi = response.content.decode('utf8')
        self.assertIn("Kegiatan", isi)
        self.assertIn("Add Kegiatan", isi)

    def test_accordion_view(self):
        response = Client().get('/accordion')
        self.assertEquals(response.status_code, 200)

    def test_accordion_content(self):
        response = Client().get('/accordion')
        isi = response.content.decode('utf8')
        self.assertIn("Accordion", isi)
        self.assertIn("Identity", isi)
        self.assertIn("Current Activity", isi)
        self.assertIn("Achievements", isi)
        self.assertIn("Organizational Experience", isi)

    def test_accordion_template_used(self):
        response = Client().get('/accordion')
        self.assertTemplateUsed(response, 'home/accordion.html')

    def test_search_view(self):
        response = Client().get('/search')
        self.assertEquals(response.status_code, 200)
        
    def test_data_view(self):
        response = Client().get('/data?q=')
        self.assertEquals(response.status_code, 200)    
        response_content = str(response.content, encoding='utf8')
        self.assertJSONEqual(
            response_content,
            {
            "error": {
                "code": 400,
                "message": "Missing query.",
                "errors": [
                {
                    "message": "Missing query.",
                    "domain": "global",
                    "reason": "queryRequired",
                    "location": "q",
                    "locationType": "parameter"
                }
                ]
            }
            }
        )